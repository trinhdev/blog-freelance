<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\DataTables\CategoryDataTable;
use App\Http\Requests\Category\CategoryRequest;
use DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class CategoryController extends Controller
{
    public function index(CategoryDataTable $dataTable)
    {
        return $dataTable->render('admin.category.index');
    }

    public function create()
    {
        return view('admin.category._add');
    }

    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        return view('admin.category._edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        
        $data = request()->validate([
            'name'        => 'required|string|max:255',
            'slug'        => 'required|string|max:255',
            'description' => 'required|string',
            'avatar'       => 'required',
            'status'      => 'required',
        ]);
        
        $category = Category::findOrFail($id);
        
        // include to save image
        if ($image = $this->upload()) {
            $category->image = $image;
        }

        $category->update([
            'name' => $data['name'],
            'slug' => $data['slug'],
            'status' => $data['status'],
            'description' => $data['description'],
            'image' => $image,
        ]);
       
        return redirect()->intended('admin/category/index')->with('success', 'Thành công');
    }

    public function store(Request $request)
    {
        
        $data = request()->validate([
            'name'        => 'required|string|max:255',
            'slug'        => 'required|string|max:255',
            'description' => 'required|string',
            'avatar'       => 'required',
            'status'      => 'required',
        ]);
        
        $category = new Category();
        
        // include to save image
        if ($image = $this->upload()) {
            $category->image = $image;
        }

        $category->create([
            'name' => $data['name'],
            'slug' => $data['slug'],
            'status' => $data['status'],
            'description' => $data['description'],
            'image' => $image,
        ]);
       
        return redirect()->intended('admin/category/index');
    }

    public function upload($folder = 'images', $key = 'avatar', $validation = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|sometimes')
    {
        request()->validate([$key => $validation]);

        $file = null;
        if (request()->hasFile($key)) {
            $file = Storage::disk('public')->putFile($folder, request()->file($key), 'public');
        }

        return $file;
    }

    //DELETE
    public function destroy($id)
    {
        $category = Category::destroy($id);
        if($category){
            Storage::disk('public')->delete($category->image);
        }
        return redirect()->back();
    }

    //UPDATE STATUS
    public function update_status($id)
    {
        $category = Category::find($id);
        if($category->status == 0)
        {
            $category->status = 1;
        }else {
            $category->status = 0;
        }
        $category->save();
        return redirect()->back();
    }

}

