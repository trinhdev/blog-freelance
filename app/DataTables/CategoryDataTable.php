<?php

namespace App\DataTables;

use App\Models\Category;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('image', function (Category $model) {
                return '<img src="'.Storage::url($model->image).'" alt="" class="symbol w-75px h-75px">';
            })
            ->editColumn('status', function (Category $model) {
                if ($model->status == 1) {
                    $style = 'danger';
                    $value = 'PRIVATE';
                }else{
                    $style = 'success';
                    $value = 'PUBLIC';
                }
                return '<div class="badge badge-light-'.$style.' fw-bolder">'.$value.'</div>';
            })
            ->rawColumns(['image','icon','status','action'])
            ->addColumn('action', 'admin.category._action-menu');
           
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('category-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters([
                        'scrollX'      => true,
                        'drawCallback' => 'function() { KTMenu.createInstances(); }',
                    ])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
                    
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('id')->title('ID')->width(100)->addClass('ps-0'),
            Column::make('name')->title('Tên danh mục'),
            Column::make('slug')->title('URL'),
            Column::make('image')->title('Hình ảnh'),
            Column::make('description')->title('Mô tả'),
            Column::make('status')->title('Trạng thái'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->title('Hành động')
                  ->addClass('text-center'),
            Column::make('created_at')->title('Ngày tạo')->addClass('none'),
            Column::make('updated_at')->title('Ngày sửa đổi')->addClass('none'),
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Category_' . date('YmdHis');
    }
}
