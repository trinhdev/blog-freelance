@extends('base.site')

@section('content')

<!-- Breadcrumb  -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Breadcrumb -->
                <ul class="breadcrumbs bg-light mb-4">
                    <li class="breadcrumbs__item">
                        <a href="index.html" class="breadcrumbs__url">
                            <i class="fa fa-home"></i> Trang chủ</a>
                    </li>
                    <li class="breadcrumbs__item">
                        <a href="index.html" class="breadcrumbs__url">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb  -->

<!-- Form contact -->
<section class="wrap__contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h5>Liên hệ với chúng tôi</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-name">
                            <label>Tên của bạn <span class="required"></span></label>
                            <input type="text" class="form-control" name="name" required="">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-name">
                            <label>Email của bạn <span class="required"></span></label>
                            <input type="email" class="form-control" name="email" required="">

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group form-group-name">
                            <label>website <span class="required"></span></label>
                            <input type="text" class="form-control" name="website" required="">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-name">
                            <label>Tiêu đề<span class="required"></span></label>
                            <input type="text" class="form-control" name="subject" required="">

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tin nhắn của bạn </label>
                            <textarea class="form-control" rows="8" name="message"></textarea>
                        </div>
                        <div class="form-group float-right mb-4">
                            <button type="submit" class="btn btn-primary">Gửi</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <h5>Thông tin vị trí</h5>
                <div class="wrap__contact-form-office">
                    <ul class="list-unstyled">
                        <li>
                            <span>
                                <i class="fa fa-home"></i>
                            </span>

                            Quan 12, TP.HCM

                        </li>
                        <li>
                            <span>
                                <i class="fa fa-phone"></i>
                                <a href="tel:">(+84) 123 456 789</a>
                            </span>

                        </li>
                        <li>
                            <span>
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:">mail@example.com</a>
                            </span>

                        </li>
                        <li>
                            <span>
                                <i class="fa fa-globe"></i>
                                <a href="#" target="_blank"> www.thanhduy.com</a>
                            </span>
                        </li>
                    </ul>

                    <div class="social__media">
                        <h5>Liên lạc với chúng tôi</h5>
                        <ul class="list-inline">

                            <li class="list-inline-item">
                                <a href="#" class="btn btn-social rounded text-white facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-social rounded text-white twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-social rounded text-white whatsapp">
                                    <i class="fa fa-whatsapp"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-social rounded text-white telegram">
                                    <i class="fa fa-telegram"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-social rounded text-white linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Form contact  -->
@endsection