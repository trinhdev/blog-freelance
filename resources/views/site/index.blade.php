@extends('base.site')

@section('content')

<div class="slider-container">
    <div class="container-slider-image-full nopadd">

        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators d-none">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ asset('assets/images/placeholder/banner.jpg') }}" alt="First slide">
                    <div class="carousel-caption d-md-block text-center text-capitalize">

                        <h1 class="text-white animated fadeInUp nopadd" style="animation-delay:1s">Cuộc đua bơm vũ khí có thể định đoạt chiến sự Ukraine</h1>
                        <p class="text-white animated fadeInDown text-center" style="animation-delay:2s">Cuộc đua bơm vũ khí có thể định đoạt chiến sự Ukraine
Cục diện chiến trường hoàn toàn mới, khi Nga xoay trục giao tranh sang miền đông Ukraine, thúc đẩy phương Tây tăng cường cung cấp vũ khí hạng nặng cho Kiev </p>
                        <div class="animated fadeInLeft d-none d-sm-block" style="animation-delay:2.6s">
                            <a href="#" class="btn btn-primary text-uppercase"> Xem thêm</a>
                        </div>

                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('assets/images/placeholder/banner1.jpg') }}" alt="Second slide">
                    <div class="carousel-caption d-md-block text-center text-capitalize">

                        <h1 class="text-white animated fadeInUp nopadd" style="animation-delay:1s">
                        Hành trình bí mật đến phút chót của Thủ tướng Anh tới Kiev </h1>
                        
                        <div class="animated fadeInLeft d-none d-sm-block" style="animation-delay:2.6s">
                            <a href="#" class="btn btn-primary text-uppercase"> Xem thêm</a>
                        </div>

                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('assets/images/placeholder/banner2.jpg') }}" alt="Third slide">
                    <div class="carousel-caption d-md-block text-center text-capitalize">

                        <h1 class="text-white animated zoomInDown nopadd" style="animation-delay:1s">
                        Cảnh sát Ukraine tuần tra ở làng Dmytrivka, phía tây Ukraine ngày 2/4, sau khi các lực lượng Nga rút khỏi khu vực phía bắc Kiev </h1>
                
                        <div class="animated fadeInLeft d-none d-sm-block " style="animation-delay:2.6s">
                            <a href="#" class="btn btn-primary text-uppercase"> Xem thêm</a>
                        </div>
                    </div>

                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <i class="fa fa-2x fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <i class="fa fa-2x fa-angle-right"></i>
            </a>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<!-- Blog carousel -->
</header>
<!-- End Header news -->

<!-- Popular Blog -->
<section class="wrap__section popular__blog-top bg-light ">
    <div class="container">
        <!-- Title head -->
        <div class="title-head">
            <div class="row justify-content-center">
                <div class="col-md-6 col-sm-12 text-center">
                    <h3>
                        Thể loại nổi bật
                    </h3>
                    <p>Cập nhật liên tục - xu hướng - hiện đại</p>
                </div>
            </div>
        </div>
        <!-- End Title head -->
        <div class="row">
            @foreach($category->take(3) as $categoryNew)
            <div class="col-lg-4">
                <!-- Post Article -->
                <div class="card__post__transition mt-4">
                    <div class="card__post__body card__post-height ">
                        <a href="#">
                            <img src="{{ Storage::url($categoryNew->image) }}" class="img-fluid w-100" alt="">
                        </a>
                        <div class="card__post__content bg__post-cover bg__post-cover-height ">
                            <div class="card__post__category text-capitalize">
                            {{  $categoryNew->name }}
                            </div>
                            <div class="card__post__title">
                                <h5>
                                    <a href="#" class="text-white">{{  $categoryNew->description }}</a>
                                </h5>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End Popular Blog -->

<!-- Diary Blog -->
<section class="wrap__section popular__blog-diary ">
    <div class="container">
        <!-- Title head -->
        <div class="title-head">
            <div class="row justify-content-center">
                <div class="col-md-6 col-sm-12 text-center">
                    <h3>
                        Bài viết mới nhất
                    </h3>
                    <p>Cập nhật liên tục - xu hướng - hiện đại</p>

                </div>
            </div>
        </div>
        <!-- End Title head -->
        <div class="row">
            <div class="col-lg-8">
                <!-- Post Article -->
                <div class="article__entry-new ">
                    <div class="article__category">
                        {{ $post->first()->category->name }}
                    </div>
                    <div class="article__image articel__image__transition">
                        <a href="#">
                            <img width="800" height="500" src="{{ Storage::url($post->first()->slide_url) }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="articel__content">

                        <div class="article__post__title title-lg">
                            <h4><a href="#">
                            {{ Str::limit($post->first()->title) }}
                                </a>
                            </h4>
                            <ul class="list-inline article__post__author">
                                <li class="list-inline-item">
                                    <span> by Admin</span>
                                </li>
                                <li class="list-inline-item">
                                    <span>{{ $post->first()->created_at->toFormattedDateString() }}</span>
                                </li>
                            </ul>
                            <p>
                            {{ Str::limit($post->first()->content) }}
                            </p>
                        </div>
                    </div>

                </div>
                <div class="row">
                    @foreach($post->chunk(2)->take(2) as $post_news)
                    <div class="col-lg-6">
                        @foreach($post_news->take(2) as $value)
                            <!-- Post Article -->
                            <div class="article__entry-new">
                                <div class="article__category">
                                {{ $value->category->name }}
                                </div>
                                <div class="article__image articel__image__transition">
                                    <a href="{{ route('article-detail', [$value->id]) }}">
                                        <img width="500" height="400" src="{{ Storage::url($value->slide_url) }}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="articel__content">

                                    <div class="article__post__title">
                                        <h5><a href="{{ route('article-detail', [$value->id]) }}">
                                            {{ Str::limit($value->title) }}
                                            </a>
                                        </h5>
                                        <ul class="list-inline article__post__author">
                                            <li class="list-inline-item">
                                                <span> by Admin</span>
                                            </li>
                                            <li class="list-inline-item">
                                                <span>{{ $value->created_at->toFormattedDateString() }}</span>
                                            </li>
                                        </ul>

                                    </div>
                                </div>

                            </div>
                        
                        @endforeach
                        </div>
                    @endforeach
                    
                </div>
                <!-- Subscribe Blog -->
                <!-- Form subscribe -->
                <div class="widget__form-subscribe bg__card-shadow mt-4">
                    <h5>

                    Các sự kiện và tin tức thế giới quan trọng nhất trong ngày.
                    </h5>
                    <p class="text-left">Nhận Bản Tin Hàng Ngày Của Magzrenvi Trên Hộp Thư Đến Của Bạn.</p>
                    <div class="input-group ">
                        <input type="text" class="form-control" placeholder="Your email address">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">Đăng kí</button>
                        </div>
                    </div>
                </div>
                <!-- End Subscribe Blog -->
                <aside class="wrapper__list__article">
                    <div class="wrapp__list__article-responsive">
                        @foreach($post->take(4) as $value_important)
                        <!-- Post Article List -->
                        <div class="card__post card__post-list card__post__transition mt-30">
                            <div class="row ">
                                <div class="col-md-5">
                                    <div class="card__post__transition">
                                        <a href="{{ route('article-detail', [$value_important->id]) }}">
                                            <img width="500" height="400" src="{{ Storage::url($value_important->slide_url) }}" class="img-fluid w-100" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-7 my-auto pl-0">
                                    <div class="card__post__body ">
                                        <div class="card__post__content  ">
                                            <div class="card__post__category ">
                                                {{ $value_important->category->name}}
                                            </div>
                                            <div class="card__post__author-info mb-2">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item">
                                                        <span class="text-primary">
                                                            by Admin
                                                        </span>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <span class="text-dark text-capitalize">
                                                        {{ $value_important->created_at->toFormattedDateString() }}
                                                        </span>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="card__post__title">
                                                <h5>
                                                    <a href="{{ route('article-detail', [$value_important->id]) }}">
                                                    {{ Str::limit($value_important->title) }}
                                                    </a>
                                                </h5>
                                                <p class="d-none d-lg-block d-xl-block mb-0">
                                                {{ Str::limit($value_important->content) }}
                                                </p>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </aside>
            </div>

            <div class="col-lg-4">
                <div class="sticky-top">
                    @foreach($post->take(2) as $pp)
                    <!-- Post Article -->
                    <div class="article__entry-new">
                        <div class="article__category">
                            {{ $pp->category->name }}
                        </div>
                        <div class="article__image articel__image__transition">
                            <a href="{{ route('article-detail', [$pp->id]) }}">
                                <img width="500" height="400" src="{{ Storage::url($pp->slide_url) }}" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="articel__content">

                            <div class="article__post__title">
                                <h5><a href="{{ route('article-detail', [$pp->id]) }}">
                                {{ Str::limit($value_important->title) }}
                                    </a>
                                </h5>
                                <ul class="list-inline article__post__author">
                                    <li class="list-inline-item">
                                        <span> by Admin</span>
                                    </li>
                                    <li class="list-inline-item">
                                        <span>{{ $post->first()->created_at->toFormattedDateString() }}</span>
                                    </li>
                                </ul>

                            </div>
                        </div>

                    </div>
                    @endforeach

                    <!-- social media -->
                    <aside class="wrapper__list__article mt-3">
                        <h4 class="border_section">Giữ kết nối</h4>
                        <!-- widget Social media -->
                        <div class="wrap__social__media">
                            <a href="#" target="_blank">
                                <div class="social__media__widget facebook">
                                    <span class="social__media__widget-icon">
                                        <i class="fa fa-facebook"></i>
                                    </span>
                                    <span class="social__media__widget-counter">
                                        19,243 fans
                                    </span>
                                    <span class="social__media__widget-name">
                                        like
                                    </span>
                                </div>
                            </a>
                            <a href="#" target="_blank">
                                <div class="social__media__widget twitter">
                                    <span class="social__media__widget-icon">
                                        <i class="fa fa-twitter"></i>
                                    </span>
                                    <span class="social__media__widget-counter">
                                        2.076 followers
                                    </span>
                                    <span class="social__media__widget-name">
                                        follow
                                    </span>
                                </div>
                            </a>
                            <a href="#" target="_blank">
                                <div class="social__media__widget youtube">
                                    <span class="social__media__widget-icon">
                                        <i class="fa fa-youtube"></i>
                                    </span>
                                    <span class="social__media__widget-counter">
                                        15,200 followers
                                    </span>
                                    <span class="social__media__widget-name">
                                        subscribe
                                    </span>
                                </div>
                            </a>

                        </div>
                    </aside>
                    <!-- End social media -->

                    <!-- Gallery About me -->
                    <aside class="wrapper__list__article ">
                        <h4 class="border_section">Thư viện</h4>
                        <!-- Gallery grid -->
                        <div class="wrap__gallery">
                            <ul class="list-inline">
                                @foreach($post->take(8) as $gal)
                                <li class="list-inline-item">
                                    <a href="#">
                                        <img width="180" height="180" src="{{ Storage::url($gal->slide_url) }}" alt="">
                                        <span class="icon_insta">
                                            <i class="fa fa-instagram"></i>
                                        </span>
                                    </a>
                                </li>
                                @endforeach

                            </ul>
                        </div>

                    </aside>
                    <!-- End Gallery About me -->

                    <!-- Archive category -->
                    <aside class=" wrapper__list__article">
                        <!-- Widget Category -->
                        <div class="widget widget__archive">
                            <div class="widget__title">
                                <h6 class="text-white mb-0">Thể loại</h6>
                            </div>
                            <ul class="list-unstyled bg__card-shadow">
                                @foreach($category->take(6) as $getName)
                                <li>
                                    <a href="category">
                                        {{ $getName->name}}
                                        <span class="badge badge-primary">{{ $getName->post->count()}}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </aside>
                    <!-- End Archive category -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<!-- End Diary Blog -->

<!-- Blog category -->
<section class="wrap__section bg-light">
    <div class="container">
        <div class="title-head">
            <div class="row justify-content-center">
                <div class="col-md-6 col-sm-12 text-center">
                    <h3>
                        Bài viết nổi bật
                    </h3>

                </div>
            </div>

        </div>
        @foreach($post->chunk(3)->take(2) as $post_fe)
            <div class="row">
                @foreach($post_fe as $fe)
                    <div class="col-lg-4">
                        <!-- Post Article -->
                        <div class="article__entry-new">
                            <div class="article__category">
                                {{$fe->category->name}}
                            </div>
                            <div class="article__image articel__image__transition">
                                <a href="{{ route('article-detail', [$fe->id]) }}">
                                    <img width="500" height="400" src="{{ Storage::url($fe->slide_url) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                            <div class="articel__content">

                                <div class="article__post__title">
                                    <h5><a href="{{ route('article-detail', [$fe->id]) }}">
                                            {{ Str::limit($fe->title) }}
                                        </a>
                                    </h5>
                                    <ul class="list-inline article__post__author">
                                        <li class="list-inline-item">
                                            <span> by Admin</span>
                                        </li>
                                        <li class="list-inline-item">
                                            <span>{{ $fe->first()->created_at->toFormattedDateString() }}</span>
                                        </li>
                                    </ul>

                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
        
    </div>
</section>
<!-- End Blog category -->


@endsection